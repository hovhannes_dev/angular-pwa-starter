import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
  menuItems: any[] =[
    {
      title: 'Home',
      url: '/'
    },
    {
      title: 'Contact Us',
      url: 'contact-us'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
