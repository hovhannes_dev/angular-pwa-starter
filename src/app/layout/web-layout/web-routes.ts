import {WebLayoutComponent} from "./web-layout.component";

export const routes = [
  {
    path: '', component: WebLayoutComponent
  }
];
