import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebLayoutComponent } from './web-layout.component';
import {RouterModule} from "@angular/router";
import {routes} from "./web-routes";
import {PageHeaderComponent} from './page-header/page-header.component';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    NgbTabsetModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WebLayoutComponent, PageHeaderComponent],
  entryComponents: [WebLayoutComponent, PageHeaderComponent],
  exports: [WebLayoutComponent, PageHeaderComponent]
})
export class WebLayoutModule { }
