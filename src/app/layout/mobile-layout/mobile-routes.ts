import {MobileLayoutComponent} from "./mobile-layout.component";

export const routes = [
  {
    path: '', component: MobileLayoutComponent
  }
]
