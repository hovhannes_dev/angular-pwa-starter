import { MobileLayoutModule } from './mobile-layout.module';

describe('MobileLayoutModule', () => {
  let mobileLayoutModule: MobileLayoutModule;

  beforeEach(() => {
    mobileLayoutModule = new MobileLayoutModule();
  });

  it('should create an instance', () => {
    expect(mobileLayoutModule).toBeTruthy();
  });
});
