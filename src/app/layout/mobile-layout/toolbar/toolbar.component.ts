import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'mobile-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Output() onBackBtnClick: EventEmitter<any> = new EventEmitter();
  @Output() onMenuToggle: EventEmitter<any> = new EventEmitter();

  menuIsOpen: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  toggleMenu() {
    this.menuIsOpen = !this.menuIsOpen;
    this.onMenuToggle.emit(this.menuIsOpen);
  }
}
