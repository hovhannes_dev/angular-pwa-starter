import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileLayoutComponent } from './mobile-layout.component';
import {RouterModule} from "@angular/router";
import {routes} from "./mobile-routes";
import { NavbarComponent } from './navbar/navbar.component';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    NgbTabsetModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MobileLayoutComponent, NavbarComponent, ToolbarComponent],
  exports: [MobileLayoutComponent],
  entryComponents: [MobileLayoutComponent],
})
export class MobileLayoutModule { }
