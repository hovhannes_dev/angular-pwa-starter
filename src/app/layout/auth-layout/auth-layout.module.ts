import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLayoutComponent } from './auth-layout.component';
import {PageNotFoundComponent} from '../../page-not-found/page-not-found.component';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundModule} from '../../page-not-found/page-not-found.module';
import {LoginPageComponent} from "../../pages/auth/login-page/login-page.component";
import {SignUpPageComponent} from "../../pages/auth/sign-up-page/sign-up-page.component";
import {ForgotPageComponent} from "../../pages/auth/forgot-page/forgot-page.component";
import {ResetPageComponent} from "../../pages/auth/reset-page/reset-page.component";
import {ReactiveFormsModule} from "@angular/forms";
const routes: Routes = [
  {
    path: '', component: AuthLayoutComponent, children: [
    { path: '', pathMatch: "full", redirectTo: "login", data: { title: 'Login' } },
    { path: 'login', component: LoginPageComponent, data: { title: 'Login' } },
    { path: 'sign-up', component: SignUpPageComponent, data: { title: 'Sign-up' } },
    { path: 'forgot', component: ForgotPageComponent, data: { title: 'Forgot Password' } },
    { path: 'reset', component: ResetPageComponent, data: { title: 'Reset Password' } },
  ]
  },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    PageNotFoundModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  declarations: [
    AuthLayoutComponent,
    LoginPageComponent,
    SignUpPageComponent,
    ForgotPageComponent,
    ResetPageComponent
  ],
  exports: [
    AuthLayoutComponent,
    LoginPageComponent,
    SignUpPageComponent,
    ForgotPageComponent,
    ResetPageComponent
  ],
  entryComponents: [
    AuthLayoutComponent,
    LoginPageComponent,
    SignUpPageComponent,
    ForgotPageComponent,
    ResetPageComponent],

})
export class AuthLayoutModule { }
