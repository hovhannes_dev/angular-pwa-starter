import { Injectable } from '@angular/core';
import { CanLoad, Router, Route } from '@angular/router';

@Injectable()
export class DisabledGuard implements CanLoad {

    constructor(protected router: Router) {
    }

  /**
   * Check if can load
   * @param route
   * @return boolean
   */
  canLoad(route: Route): boolean {
        return false;
    }

}
