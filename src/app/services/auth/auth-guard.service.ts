import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  NavigationExtras,
  CanLoad, Route
} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private angularFire: AngularFireAuth, private router: Router, private authService: AuthService) {

  }

  /**
   * Check if can activate route
   * @param route
   * @param state
   * @return boolean
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    return this.checkLogin(url);
  }

  /**
   * Check if can activate child route
   * @param route
   * @param state
   * @return boolean
   */
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  /**
   * Check if can load route
   * @param route
   * @return boolean
   */
  canLoad(route: Route): boolean {
    const url = `/${route.path}`;
    return this.checkLogin(url) && this.checkRoles(route);
  }

  /**
   * Check if is logged in
   * @param url
   * @return boolean
   */
  checkLogin(url: string): boolean {
    if (this.authService.user && this.authService.user.uid) {
      return true;
    } else {
      // Navigate to the login page with extras
      this.router.navigate(['/auth/login']);
      return false;
    }
    // Store the attempted URL for redirecting
  }

  /**
   * Check roles
   * @param route
   * @return boolean
   */
  checkRoles(route: Route): boolean {
    if (route.data && route.data.accessRoles) {
      const index = route.data.accessRoles.findIndex((i) => i === this.authService.user.role);
      return index > -1;
    } else {
      return true;
    }
  }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
