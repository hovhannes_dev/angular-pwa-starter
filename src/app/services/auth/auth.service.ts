import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Utils} from '../../shared/Utils';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabase} from 'angularfire2/database';
import {UserInterface} from '../../interfaces/user.interfaces';

@Injectable()
export class AuthService {

  private _user: any;
  private usersCollection: AngularFirestoreCollection<UserInterface>;

  constructor( private afStore: AngularFirestore,
               private afAuth: AngularFireAuth,
               private db: AngularFireDatabase) {
    this.usersCollection = this.afStore.collection('users');
    if (this.user) {
      // this.userStatusChange();
    }
  }

  /**
   * get User value
   */
  get user(): any {
    const user = JSON.parse(localStorage.getItem('logged_user'));
    return user ? user : null;
  }

  /**
   * Set user value
   * @param value
   */
  set user(value: any) {
    if (value && localStorage !== undefined) {
      const userData = JSON.stringify(Utils.copy(value));
      localStorage.setItem('logged_user', userData);
    }
    this._user = value;
  }

  login(user): void {
    if (user) {
      this.user = user;
      // this.userStatusChange();
    }
  }
  userStatusChange(): void {
    const onlineRef = this.db.database.ref('.info/connected'); // Get a reference to the list of connections
    onlineRef.on('value', snapshot => {
      this.db.database.ref(`/status/${this.user.uid}`)
        .onDisconnect() // Set up the disconnect hook
        .update({status: 'offline'}) // The value to be set for this key when the client disconnects
        .then(() => {
          // Set the Firestore User's online status to true
          this.usersCollection.doc(this.user.uid).update({ isOnline: true }).catch((e)=>{
            console.error(e);
          });

          // Let's also create a key in our real-time database
          // The value is set to 'online'
          this.db.database.ref(`/status/${this.user.uid}`).update({status: 'online'}).catch((e)=>{
            console.error(e);
          });
        });
    });
  }

  /**
   * Check login
   * @return boolean
   */
  checkLogin(): boolean {
    return this.user !== null;
  }

  /**
   * Logout
   */
  logout(): void {
    this.afAuth.auth.signOut().then(() => {
      this.user = null;
      sessionStorage.clear();
      localStorage.clear();
    });

  }

  /**
   * Sign out
   * @return Promise<boolean>
   */
  signOut(): Promise<boolean> {
    return new Promise<boolean>(resolve => {
        this.logout();
        resolve(true);

    });
  }
}
