import { NgModule }             from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';

import { CanDeactivateGuard }       from './services/auth/can-deactivate-guard.service';
import { AuthGuard }                from './services/auth/auth-guard.service';
import { SelectivePreloadingStrategy } from './shared/SelectivePreloadingStrategy';
import {AppComponent} from './app.component';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from './services/auth/auth.service';
import {Utils} from "./shared/Utils";
import {DisabledGuard} from "./services/auth/disabled-guard";


export const webRoutes: Routes = [
  {
    path: 'auth',
    loadChildren: 'app/layout/auth-layout/auth-layout.module#AuthLayoutModule',
    data: { title: 'Login' }
  },
  {
    path: '',
    loadChildren: 'app/layout/web-layout/web-layout.module#WebLayoutModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'mobile',
    loadChildren: 'app/layout/mobile-layout/mobile-layout.module#MobileLayoutModule',
    canLoad: [DisabledGuard]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: "full"
  }
];
export const mobileRoutes: Routes = [
  {
    path: 'auth',
    loadChildren: 'app/layout/auth-layout/auth-layout.module#AuthLayoutModule',
    data: { title: 'Login' }
  },
  {
    path: '',
    loadChildren: 'app/layout/mobile-layout/mobile-layout.module#MobileLayoutModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'web',
    loadChildren: 'app/layout/web-layout/web-layout.module#WebLayoutModule',
    canLoad: [DisabledGuard]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: "full"
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      webRoutes,
      {
        useHash: true,
        enableTracing: false, // <-- debugging purposes only
        preloadingStrategy: SelectivePreloadingStrategy,

      }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService,
    AngularFireAuth,
    AuthGuard,
    CanDeactivateGuard,
    SelectivePreloadingStrategy
  ]
})
export class AppRoutingModule {
  constructor(router: Router) {
    if (Utils.isMobile()) {
      router.resetConfig(mobileRoutes);
    } else {
      router.resetConfig(webRoutes);
    }
  }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
