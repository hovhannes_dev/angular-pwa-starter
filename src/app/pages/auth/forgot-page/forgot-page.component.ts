import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-forgot-page',
  templateUrl: './forgot-page.component.html',
  styleUrls: ['./forgot-page.component.scss']
})
export class ForgotPageComponent implements OnInit {

  forgotForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.forgotForm = this.fb.group({
      'email': new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$'),
        Validators.minLength(4),
      ])
    });
  }

  sendEmail(e) {
    // Todo implement send email
  }

}
