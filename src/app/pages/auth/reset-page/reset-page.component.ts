import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-reset-page',
  templateUrl: './reset-page.component.html',
  styleUrls: ['./reset-page.component.scss']
})
export class ResetPageComponent implements OnInit {

  resetForm: FormGroup;
  resetToken: string;

  constructor(private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit() {
    this.resetToken = this.route.snapshot.queryParams['resetToken'];

    this.resetForm = this.fb.group({
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      'confirm': new FormControl('', [

      ])
    });
  }

  sendEmail(e) {
    // Todo implement send email logic
  }

}
