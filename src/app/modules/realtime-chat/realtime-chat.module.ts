import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RealtimeChatComponent } from './realtime-chat.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RealtimeChatComponent]
})
export class RealtimeChatModule { }
