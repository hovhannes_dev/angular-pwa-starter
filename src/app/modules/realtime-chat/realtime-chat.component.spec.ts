import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtimeChatComponent } from './realtime-chat.component';

describe('RealtimeChatComponent', () => {
  let component: RealtimeChatComponent;
  let fixture: ComponentFixture<RealtimeChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtimeChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtimeChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
