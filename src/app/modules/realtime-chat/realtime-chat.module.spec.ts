import { RealtimeChatModule } from './realtime-chat.module';

describe('RealtimeChatModule', () => {
  let realtimeChatModule: RealtimeChatModule;

  beforeEach(() => {
    realtimeChatModule = new RealtimeChatModule();
  });

  it('should create an instance', () => {
    expect(realtimeChatModule).toBeTruthy();
  });
});
