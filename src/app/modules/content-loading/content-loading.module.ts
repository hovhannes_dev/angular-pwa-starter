import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentLoadingComponent } from './content-loading.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ContentLoadingComponent],
  entryComponents: [ContentLoadingComponent],
  exports: [ContentLoadingComponent]
})
export class ContentLoadingModule { }
