import { ContentLoadingModule } from './content-loading.module';

describe('ContentLoadingModule', () => {
  let contentLoadingModule: ContentLoadingModule;

  beforeEach(() => {
    contentLoadingModule = new ContentLoadingModule();
  });

  it('should create an instance', () => {
    expect(contentLoadingModule).toBeTruthy();
  });
});
