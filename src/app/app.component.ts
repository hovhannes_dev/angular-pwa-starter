import {Component} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Angular PWA Starter';
  splashScreen: boolean = true;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private titleService: Title){}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        let title = this.getTitle(this.activatedRoute.snapshot.root);
        this.titleService.setTitle(title);
      }
    });
  }

  onActivateRoute(){
    this.splashScreen = false;
  }

  private getTitle(routeSnapshot: ActivatedRouteSnapshot, title: string = '') {
    title = routeSnapshot.data && routeSnapshot.data['title'] ? routeSnapshot.data['title'] : title;
    if (routeSnapshot.firstChild) {
      title = this.getTitle(routeSnapshot.firstChild, title);
    }
    return title;
  }
}
