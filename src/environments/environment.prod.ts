export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyA4EH3QaQdr0i6sj3m_cUev7wirB27JAQs",
    authDomain: "angular-pwa-starter.firebaseapp.com",
    databaseURL: "https://angular-pwa-starter.firebaseio.com",
    projectId: "angular-pwa-starter",
    storageBucket: "angular-pwa-starter.appspot.com",
    messagingSenderId: "30358101416"
  }
};
