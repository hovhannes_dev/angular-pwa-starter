// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA4EH3QaQdr0i6sj3m_cUev7wirB27JAQs",
    authDomain: "angular-pwa-starter.firebaseapp.com",
    databaseURL: "https://angular-pwa-starter.firebaseio.com",
    projectId: "angular-pwa-starter",
    storageBucket: "angular-pwa-starter.appspot.com",
    messagingSenderId: "30358101416"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
